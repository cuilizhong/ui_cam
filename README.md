# UI_Cam

[![CI Status](http://img.shields.io/travis/Cui lizhong/UI_Cam.svg?style=flat)](https://travis-ci.org/Cui lizhong/UI_Cam)
[![Version](https://img.shields.io/cocoapods/v/UI_Cam.svg?style=flat)](http://cocoapods.org/pods/UI_Cam)
[![License](https://img.shields.io/cocoapods/l/UI_Cam.svg?style=flat)](http://cocoapods.org/pods/UI_Cam)
[![Platform](https://img.shields.io/cocoapods/p/UI_Cam.svg?style=flat)](http://cocoapods.org/pods/UI_Cam)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UI_Cam is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "UI_Cam"
```

## Author

Cui lizhong, cui_li_zhong@163.com

## License

UI_Cam is available under the MIT license. See the LICENSE file for more info.
