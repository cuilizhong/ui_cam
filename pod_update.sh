#!/bin/bash

checkExec() {
  if [ $? -eq 0 ]; then
    # success
    echo "success"
  else
    # fail
    echo "fail"
    exit 0
  fi
}

updatePod() {
  echo -e "\n\033[34mget your pod version ...\033[0m"
  pod --version
  checkExec

  echo -e "\nbegin update local pod repo ...\033[0m"
  pod repo update CLZSpecs
  checkExec

  # pod update --project-directory=Example --verbose
  # 这里的Example为相对路径，后面改为绝对路径
  echo -e "\nbegin update ...\033[0m"
  pod update --verbose --no-repo-update --project-directory=Example
  checkExec
}

updatePod
