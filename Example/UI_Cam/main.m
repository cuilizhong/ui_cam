//
//  main.m
//  UI_Cam
//
//  Created by Cui lizhong on 12/21/2020.
//  Copyright (c) 2020 Retechcorp. All rights reserved.
//

@import UIKit;
#import "CLZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CLZAppDelegate class]));
    }
}
