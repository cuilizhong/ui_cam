//
//  CLZAppDelegate.h
//  UI_Cam
//
//  Created by Cui lizhong on 12/21/2020.
//  Copyright (c) 2020 Retechcorp. All rights reserved.
//

@import UIKit;

@interface CLZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
